using Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica.Interfaces
{
    public interface ILeerTexto
    {
        List<Operacion> LstOperaciones { get; }
        void RealizarLectura(string p_texto);
    }
}