using Logica.Interfaces;
using Logica.Servicio;
using webapi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;



namespace webapi.Controllers
{
    [Route("api/Operacion")]
     public class OperacionController : Controller
    {
        private readonly IOperacion _Ioperacion = new SOperacion();

        [HttpPost]
        [ Route("GetResult")]
        public IActionResult ObtenerResultado(Operacion operacion)
        {
            try
            {
                List<string> _result = _Ioperacion.Realizar(p_texto: operacion.operacion);
                var _anonymousType = new { result = _result };
                return Ok();
            }
            catch (Exception ex)
            {
             return BadRequest(ex.Message);
            }
        }

        
        

    }
}