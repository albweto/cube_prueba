import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Operacion } from '../models/operacion'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) {}

  ObtenerResultado(_operacion:Operacion):Observable<any>{
    return this.http.post("https://localhost:5001api/Operacion/GetResult",_operacion); 
  }
}
