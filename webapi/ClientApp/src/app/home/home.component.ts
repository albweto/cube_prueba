import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Operacion } from '../models/operacion'
import { ApiService } from '../service/api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {

  showError:boolean= false; 
  textError:string = ""; 
/*
  
  */
  
  operacion = new FormGroup({
    txtdato: new FormControl('')
  }); 

  txtdatoResult = new FormControl(''); 
  
  constructor(private servicio: ApiService) { }

  ngOnInit() {
  }

   onSubmit(){
    if(this.operacion.valid){

      let op = new Operacion(); 
      op.operacion = this.operacion.controls.txtdato.value

      this.servicio.ObtenerResultado(op).subscribe(
        (data)=>{
          let _result:string = ""; 
          data.result.forEach(element => {
             _result += element+"\n"; 
          });
          this.txtdatoResult.setValue(_result); 
        },
        (e) => {

            this.showError = true; 
            this.textError = e.error; 
            setTimeout(() => {
              this.showError = false; 
              this.textError = "";  
            }, 5000);
        }); 
      
    }
}
}
