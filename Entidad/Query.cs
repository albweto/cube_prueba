namespace Entidad
{
    public class Query
    {
        public int x0 { get; set; }
        public int x1 { get; set; }
        public int y0 { get; set; }
        public int y1 { get; set; }
        public int z0 { get; set; }
        public int z1 { get; set; }
    }
}