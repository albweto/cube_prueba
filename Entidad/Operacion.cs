using Entidad.Enum;

namespace Entidad
{
    public class Operacion {
    public int? NumeroCasos { get; set; }
    public int? TamañoMatrix { get; set; }
    public int? NumeroOperaciones { get; set; }
    public Comandos Comando { get; set; }
    public Update Update { get; set; }
    public Query Query { get; set; }

    public Operacion()
    {
        NumeroCasos = null;
        TamañoMatrix = null;
        NumeroOperaciones = null;
        Comando = Comandos.INVALID;
        Update = new Update();
        Query = new Query();
    }
    }
}
